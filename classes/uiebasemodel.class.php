<?php

class UIEBaseModel {
  protected $mMode;
  protected $mValidated = false;
  protected $mErrorCounter = 0;
  protected $mErrors = false;
  protected $mTableName;

/*
  public function __call($pMethod, $pArgs)
  {
    $vGetSet = strtolower(substr($pMethod, 0, 3));
    $vVarName = 'm'.substr($pMethod, 3);
    switch($vGetSet) {
      case 'get':
        return $this->{$vVarName};
        break;
      case 'set':
        $vValue = array_pop($pArgs);
        $this->{$vVarName} = $vValue;
        break;
    }
  }
*/
  public function GetById() { $this->AddError('No GetById Function defined'); }
  public function GetArrayValues() { $this->AddError('No GetArrayValues Function defined'); }
  public function Maintain() { $this->AddError('No Maintain Function defined'); }
  public function Validate() { $this->AddError('No Validate Function defined'); }
  public function Search() { $this->AddError('No Search Function defined'); }
  public function InitFromResults($pResultObj) { $this->AddError('No InitFromResults Function defined'); }

  public function GetMode() {
    return $this->mMode;
  }

  public function SetMode($pMode) {
    $this->mMode = $pMode;
  }

  public function NoErrors() {
    return $this->mErrorCounter == 0;
  }

  public function GetErrors() {
    return $this->mErrors;
  }

  public function AddError($pError) {
    if(!$this->mErrors) {
      $this->mErrors = true;
    }
    $vThisClass = get_class($this);
    form_set_error($vThisClass . $this->mErrorCounter++, $vThisClass . ' - ' . t($pError));
  }

  public function GetTableName() {
    return $this->mTableName;
  }

  public function IsValidated() {
    return $this->mValidated;
  }
}
