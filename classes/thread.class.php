<?php

/**
 * Thread Object
 */

class UIEThread extends UIEBaseModel {
  protected $mId;
  protected $mTitle;
  protected $mCreatorId;
  protected $mCreatedDate;
  protected $mLastPoster;
  protected $mLastPostDate;
  protected $mLocked;
  protected $mSticky;
  protected $mForumId;
  protected $mViewCount;
  protected $mPostCount;

  public function __construct() {
  }
}
