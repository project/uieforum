<?php

/**
 * History Object
 */

class UIEHistory extends UIEBaseModel {

  protected $mClassNameHuman = 'History';
  protected $mTableName = '{f_history}';

  protected $mUid;
  protected $mTid;
  protected $mTimestamp;

  public function __construct() {
  }

  public function Validate() {
    $this->mValidated = false;
    $vMode = $this->GetMode();
    switch ($vMode) {
      case UIEMODEL_MODE_ADD:
      case UIEMODEL_MODE_UPDATE:
        if(!$this->mUid) { $this->AddError('Uid not set'); }
        if(!$this->mTid) { $this->AddError('Tid not set'); }
        if(!$this->mTimestamp) { $this->AddError('Timestamp not set'); }
      break;
      case UIEMODEL_MODE_DELETE:
        if(!$this->mTimestamp) { $this->AddError('Timestamp not set'); }
      break;
      case UIEMODEL_MODE_SEARCH:
        if(!$this->mUid) { $this->AddError('Uid not set'); }
        if(!$this->mTid) { $this->AddError('Tid not set'); }
      break;
    }
    if($this->NoErrors()) {
      $this->mValidated = true;
    }
  }

  public function Maintain() {
    if(!$this->IsValidated()) {
      $this->AddError('Not validated');
      return false;
    }

    $vSuccess = false;
    $vMode = $this->GetMode();
    switch ($GLOBALS['db_type']) {
      case 'mysql':
      case 'mysqli':
        switch ($vMode) {
          case UIEMODEL_MODE_ADD:
            $vSuccess = db_query("INSERT INTO {$this->GetTableName()} (uid, ThreadID, timestamp) VALUES (%d, %d, %d)", $this->GetUid(), $this->GetTid(), $this->GetTimestamp());
            break;
          case UIEMODEL_MODE_UPDATE:
            $vSuccess = db_query("UPDATE {$this->GetTableName()} SET timestamp = %d WHERE uid = %d AND ThreadID = %d", $this->GetTimestamp(), $this->GetUid(), $this->GetTid());
            break;
          case UIEMODEL_MODE_DELETE:
            $vSuccess = db_query("DELETE FROM {$this->GetTableName()} WHERE timestamp < %d", $this->GetTimestamp());
            break;
          default:
            $this->AddError('Unknown mode: '.$vMode);
          break;
        }
        break;
    }
    return $vSuccess ? true : false;
  }

  public function Search() {
    if(!$this->IsValidated()) {
      $this->AddError('Not validated');
      return false;
    }

    $vTempResults = db_query("SELECT timestamp FROM {$this->GetTableName()} WHERE uid = %d AND ThreadID = %d", $this->GetUid(), $this->GetTid());
    return $vTempResults;
  }

  public function GetUid() { return $this->mUid; }
  public function GetTid() { return $this->mTid; }
  public function GetTimestamp() { return $this->mTimestamp; }

  public function SetUid($pUid) {
    $this->mUid = $pUid;
  }

  public function SetTid($pTid) {
    $this->mTid = $pTid;
  }

  public function SetTimestamp($pTimestamp = false) {
    if(!$pTimestamp) {
      $pTimestamp = time();
    }
    $this->mTimestamp = $pTimestamp;
  }
}
