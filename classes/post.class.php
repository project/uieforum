<?php

/**
 * Post Object
 */

class UIEPost extends UIEBaseModel {
  protected $mId;
  protected $mTitle;
  protected $mThreadId;
  protected $mCreatorId;
  protected $mCreatedDate;
  protected $mContent;
  protected $mAlwaysEdit;
  protected $mEditedDate;
  protected $mEditorId;
  protected $mEditReason;

  public function __construct() {
  }
}
