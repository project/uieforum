<?php

/**
 * Security Group Access Object
 */

class UIESecGroupAccess extends UIEBaseModel {

  protected $mClassNameHuman = 'SecurityGroupAccess';
  protected $mTableName = '{f_groups_access}';

  protected $mGAid;
  protected $mGid;
  protected $mFid;

  public function __construct() {
  }

  public function Validate() {
    $this->mValidated = false;
    $vMode = $this->GetMode();
    switch ($vMode) {
      case UIEMODEL_MODE_ADD:
        if(!$this->mGid) { $this->AddError('Group Id not set'); }
        if(!$this->mFid) { $this->AddError('Forum Id not set'); }
      break;
      case UIEMODEL_MODE_UPDATE:
        if(!$this->mGAid) { $this->AddError('Group Access Id not set'); }
        if(!$this->mGid) { $this->AddError('Group Id not set'); }
        if(!$this->mFid) { $this->AddError('Forum Id not set'); }
      break;
      case UIEMODEL_MODE_DELETE:
        if(!$this->mFid) { $this->AddError('Forum Id not set'); }
      break;
      default:
        $this->AddError('Unknown mode: '.$vMode);
      break;
    }
    if($this->NoErrors()) {
      $this->mValidated = true;
    }
  }

  public function Maintain() {
    if(!$this->IsValidated()) {
      $this->AddError('Not validated');
      return false;
    }

    $vSuccess = false;
    $vMode = $this->GetMode();
    switch ($GLOBALS['db_type']) {
      case 'mysql':
      case 'mysqli':
        switch ($vMode) {
          case UIEMODEL_MODE_ADD:
            $vSuccess = db_query("REPLACE INTO {$this->GetTableName()} SET ForumID='%d', GroupID='%d'", $this->GetFid(), $this->GetGid());
            break;
          case UIEMODEL_MODE_DELETE:
            $vSuccess = db_query("DELETE FROM {$this->GetTableName()} WHERE ForumID=%d", $this->GetFid());
            break;
        }
        break;
    }
    return $vSuccess ? true : false;
  }

/*  public function Search() {
    if(!$this->IsValidated()) {
      $this->AddError('Not validated');
      return false;
    }

    $vTempResults = db_query("SELECT GroupShortName FROM {$this->GetTableName()} WHERE GroupID = %d AND GroupName = %d", $this->GetUid(), $this->GetTid());
    return $vTempResults;
  }*/

  public function GetGAid() { return $this->mGAid; }
  public function GetGid() { return $this->mGid; }
  public function GetFid() { return $this->mFid; }

  public function SetGAid($pGAid) {
    $this->mGAid = $pGAid;
  }

  public function SetGid($pGid) {
    $this->mGid = $pGid;
  }

  public function SetFid($pFid) {
    $this->mFid = $pFid;
  }

  public function GetById($pId) {
    $this->SetGAid($pId);
    $vObj = db_fetch_object(db_query("SELECT * FROM {$this->GetTableName()} WHERE GroupAccessID=%d", $this->GetGAid()));
    $this->SetGid($vObj->GroupID);
    $this->SetFid($vObj->ForumID);
  }

/*  public function GetArrayValues() {
    return array(
      'GroupAccessID' => $this->GetGAid()
      , 'GroupID' => $this->GetGid()
      , 'ForumID' => $this->GetFid()
    );
  }*/
}
