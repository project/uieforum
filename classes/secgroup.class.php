<?php

/**
 * Security Group Object
 */

class UIESecGroup extends UIEBaseModel {

  protected $mClassNameHuman = 'SecurityGroup';
  protected $mTableName = '{f_groups}';

  protected $mGid;
  protected $mName;
  protected $mShortName;

  public function __construct() {
  }

  public function Validate() {
    $this->mValidated = false;
    $vMode = $this->GetMode();
    switch ($vMode) {
      case UIEMODEL_MODE_ADD:
        if(!$this->mName) { $this->AddError('Name not set'); }
        if(!$this->mShortName) { $this->AddError('Shortname not set'); }
      break;
      case UIEMODEL_MODE_UPDATE:
        if(!$this->mGid) { $this->AddError('Group Id not set'); }
        if(!$this->mName) { $this->AddError('Name not set'); }
        if(!$this->mShortName) { $this->AddError('ShortName not set'); }
      break;
      case UIEMODEL_MODE_DELETE:
        if(!$this->mGid) { $this->AddError('Group Id not set'); }
      break;
      default:
        $this->AddError('Unknown mode: '.$vMode);
      break;
    }
    if($this->NoErrors()) {
      $this->mValidated = true;
    }
  }

  public function Maintain() {
    if(!$this->IsValidated()) {
      $this->AddError('Not validated');
      return false;
    }

    $vSuccess = false;
    $vMode = $this->GetMode();
    switch ($GLOBALS['db_type']) {
      case 'mysql':
      case 'mysqli':
        switch ($vMode) {
          case UIEMODEL_MODE_ADD:
            $vSuccess = db_query("INSERT INTO {$this->GetTableName()} (GroupName, GroupShortName) VALUES ('%s', '%s')", $this->GetName(), $this->GetShortName());
            if($vSuccess) {
              $query = db_query("SELECT LAST_INSERT_ID() AS id");
              $result = db_fetch_object($query);
              $this->SetGid($result->id);
            }
            break;
          case UIEMODEL_MODE_UPDATE:
            $vSuccess = db_query("UPDATE {$this->GetTableName()} SET GroupName='%s', GroupShortName='%s' WHERE GroupID=%d", $this->GetName(), $this->GetShortName(), $this->GetGid());
            break;
          case UIEMODEL_MODE_DELETE:
            $vSuccess = db_query("DELETE FROM {$this->GetTableName()} WHERE GroupID=%d", $this->GetGid());
            break;
        }
        break;
    }
    return $vSuccess ? true : false;
  }

  public function Search() {
    if(!$this->IsValidated()) {
      $this->AddError('Not validated');
      return false;
    }

    $vTempResults = db_query("SELECT GroupShortName FROM {$this->GetTableName()} WHERE GroupID = %d AND GroupName = %d", $this->GetUid(), $this->GetTid());
    return $vTempResults;
  }

  public function GetGid() { return $this->mGid; }
  public function GetName() { return $this->mName; }
  public function GetShortName() { return $this->mShortName; }

  public function SetGid($pGid) {
    $this->mGid = $pGid;
  }

  public function SetName($pName) {
    $this->mName = $pName;
  }

  public function SetShortName($pShortName) {
    $this->mShortName = $pShortName;
  }

  public function GetById($pId) {
    $this->SetGid($pId);
    $vObj = db_fetch_object(db_query("SELECT * FROM {$this->GetTableName()} WHERE GroupID=%d", $this->GetGid()));
    if(!$vObj) {
      $this->SetGid(null);
      return false;
    }
    $this->SetName($vObj->GroupName);
    $this->SetShortName($vObj->GroupShortName);

    return true;
  }

  public function GetArrayValues() {
    return array(
      'GroupID' => $this->GetGid()
      , 'GroupName' => $this->GetName()
      , 'GroupShortName' => $this->GetShortName()
    );
  }
}
