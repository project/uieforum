<?php

/**
 * Forum Object
 */

class UIEForum extends UIEBaseModel {

  protected $mClassNameHuman = 'Forum';
  protected $mTableName = '{f_forums}';

  protected $mFid;
  protected $mTitle;
  protected $mParentId;
  protected $mDescription;
  protected $mWeight;
  protected $mLocked;
  protected $mThreadCount;
  protected $mPostCount;
  protected $mThreadCountTotal;
  protected $mPostCountTotal;

  public function __construct() {
  }

  public function Validate() {
    $this->mValidated = false;
    $vMode = $this->GetMode();
    switch ($vMode) {
      case UIEMODEL_MODE_ADD:
        if(!$this->mTitle) { $this->AddError('Name not set'); }
        if(!$this->mParentId) { $this->AddError('Parent Forum not set'); }
      break;
      case UIEMODEL_MODE_UPDATE:
        if(!$this->mFid) { $this->AddError('Forum Id not set'); }
        if(!$this->mTitle) { $this->AddError('Name not set'); }
        if($this->mFid && $this->mFid != 1 && !$this->mParentId) { $this->AddError('Parent Forum not set'); }
      break;
      case UIEMODEL_MODE_SEARCH:
      case UIEMODEL_MODE_DELETE:
        if(!$this->mFid) { $this->AddError('Forum Id not set'); }
      break;
      default:
        $this->AddError('Unknown mode: '.$vMode);
      break;
    }
    if($this->NoErrors()) {
      $this->mValidated = true;
    }
  }

  public function Maintain() {
    if(!$this->IsValidated()) {
      $this->AddError('Not validated');
      return false;
    }

    $vSuccess = false;
    $vMode = $this->GetMode();
    switch ($GLOBALS['db_type']) {
      case 'mysql':
      case 'mysqli':
        switch ($vMode) {
          case UIEMODEL_MODE_ADD:
            $vSuccess = db_query("INSERT INTO {$this->GetTableName()} (ForumName, ParentForum, ForumDesc, weight, Locked) VALUES ('%s', %d, '%s', %d, %d)", $this->GetTitle(), $this->GetParentId(), $this->GetDescription(), $this->GetWeight(), $this->GetLocked());
            if($vSuccess) {
              $query = db_query("SELECT LAST_INSERT_ID() AS id");
              $result = db_fetch_object($query);
              $this->SetFid($result->id);

              if(variable_get('uieforum_auto_greet', 1)) {
                $vGreetThreadTitle = t('Welcome to ').$this->GetTitle();
                $vGreetThreadContent = t('This is the first thread in your new forum. You can disable this auto-greeting post by disabling the relevant option on the configure page.');
                uieforum_create_thread($vGreetThreadTitle, $vGreetThreadContent, $this->GetFid());
              }

            }
            break;
          case UIEMODEL_MODE_UPDATE:
            $vQuery = "UPDATE {$this->GetTableName()} SET ForumName='%s', ForumDesc='%s', weight='%d', Locked='%d', ParentForum='%d' WHERE ForumID='%d'";
            $vSuccess = db_query($vQuery, $this->GetTitle(), $this->GetDescription(), $this->GetWeight(), $this->GetLocked(), $this->GetParentId(), $this->GetFid());
            break;
          case UIEMODEL_MODE_DELETE:
            $vSuccess = db_query("DELETE FROM {$this->GetTableName()} WHERE GroupID=%d", $this->GetGid());
            break;
        }
        break;
    }
    return $vSuccess ? true : false;
  }

  public function Search() {
    if(!$this->IsValidated()) {
      $this->AddError('Not validated');
      return false;
    }

    $vTempResults = db_fetch_object(db_query("SELECT * FROM {$this->GetTableName()} WHERE ForumID = %d", $this->GetFid()));
    return $vTempResults;
  }

  public function GetFid() { return $this->mFid; }
  public function GetTitle() { return $this->mTitle; }
  public function GetParentId() { return $this->mParentId; }
  public function GetDescription() { return $this->mDescription; }
  public function GetWeight() { return $this->mWeight; }
  public function GetLocked() { return $this->mLocked; }
  public function GetThreadCount() { return $this->mThreadCount; }
  public function GetPostCount() { return $this->mPostCount; }
  public function GetThreadCountTotal() { return $this->mThreadCountTotal; }
  public function GetPostCountTotal() { return $this->mPostCountTotal; }

  public function SetFid($pId) { $this->mFid = $pId; }
  public function SetTitle($pName) { $this->mTitle = $pName; }
  public function SetParentId($pPid) { $this->mParentId = $pPid; }
  public function SetDescription($pDesc) { $this->mDescription = $pDesc; }
  public function SetWeight($pWeight) { $this->mWeight = $pWeight; }
  public function SetLocked($pLocked) { $this->mLocked = $pLocked; }
  public function SetThreadCount($pCount) { $this->mThreadCount = $pCount; }
  public function SetPostCount($pCount) { $this->mPostCount = $pCount; }
  public function SetThreadCountTotal($pCount) { $this->mThreadCountTotal = $pCount; }
  public function SetPostCountTotal($pCount) { $this->mPostCountTotal = $pCount; }

  public function GetById($pId) {
    $this->SetFid($pId);
    $vObj = db_fetch_object(db_query("SELECT * FROM {$this->GetTableName()} WHERE ForumID=%d", $this->GetFid()));
    $this->SetName($vObj->GroupName);
    $this->SetShortName($vObj->GroupShortName);
  }

  public function InitFromResults($pResultsObj) {
    $this->SetFid($pResultsObj->ForumID);
    $this->SetTitle($pResultsObj->ForumName);
    $this->SetParentId($pResultsObj->ParentForum);
    $this->SetDescription($pResultsObj->ForumDesc);
    $this->SetWeight($pResultsObj->weight);
    $this->SetLocked($pResultsObj->Locked);
    $this->SetThreadCount($pResultsObj->ThreadCount);
    $this->SetPostCount($pResultsObj->PostCount);
    $this->SetThreadCountTotal($pResultsObj->TotalThreadCount);
    $this->SetPostCountTotal($pResultsObj->TotalPostCount);
  }

  public function GetArrayValues() {
    return array(
      'ForumID' => $this->GetFid()
      , 'ForumName' => $this->GetTitle()
      , 'ParentForum' => $this->GetParentId()
      , 'ForumDesc' => $this->GetDescription()
      , 'weight' => $this->GetWeight()
      , 'Locked' => $this->GetLocked()
      , 'ThreadCount' => $this->GetThreadCount()
      , 'PostCount' => $this->GetPostCount()
      , 'TotalThreadCount' => $this->GetThreadCountTotal()
      , 'TotalPostCount' => $this->GetPostCountTotal()
    );
  }
}
