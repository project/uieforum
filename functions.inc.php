<?php
/**
 * Given a uid, return a string representing the user's rank.
 * Thanks to Morbus (IRC) for this code
 */
function uieforum_user_rank($posts = NULL)
{
  global $user;
  if ($posts == NULL || !isset($user->uid)) { return NULL; }

  switch ($posts) {
    case $posts <= 10: return t( variable_get('forum_rank_first', 'n00b') );
    case $posts <= 100:  return t( variable_get('forum_rank_second', 'Killing Spree') );
    case $posts <= 500:  return t( variable_get('forum_rank_third', 'Rampage') );
    case $posts <= 1000: return t( variable_get('forum_rank_fourth', 'Dominating') );
    case $posts <= 4000: return t( variable_get('forum_rank_fifth', 'Unstoppable') );
    case $posts <= 5000: return t( variable_get('forum_rank_sixth', 'Godlike') );
    case $posts < 7000: return t( variable_get('forum_rank_seventh', 'Wicked Sick') );
    case $posts >= 7000: return t( variable_get('forum_rank_eighth', 'Oh Emm Gee') );
  }
}

/** Returns the entire <a> html code for the current thread 
**/
function uieforum_get_thread_link_text($t, $parseUIEText = true) {
  static $vals = array();
  if(!$vals[$t->ThreadID])
  {
    $vals[$t->ThreadID] = l(uieforum_get_thread_title($t, $parseUIEText), uieforum_get_module_menu_name()."/thread/{$t->ThreadID}");
  }
  return $vals[$t->ThreadID];
}

/** Returns the link to the last post of a thread
**/
function uieforum_get_thread_link_text_last_post($t, $objstr, $postcount = false)
{
  $postcount = $postcount ? $postcount : $t->PostCount;
  $maxPage = uieforum_get_max_page_count($postcount);

	$end_of_link = "";
	$page = ($maxPage > 1 ? "page={$maxPage}" : null);
	$actual_link = uieforum_get_module_menu_name()."/thread/{$t->ThreadID}";
  if($objstr) $var = l($objstr, $actual_link, null, $page, 'lastpost', null, true);
	else {
    $var = array(
      'url' => $actual_link,
      'page' => $page,
    );
	}
  return $var;
}

function uieforum_get_thread_title($t, $parseUIEText = true) {
  return uieforum_filter('process', null, null, $t->ThreadTitle, null, $parseUIEText);
}

/** Returns the thread object that contains a specific post
**/
function uieforum_get_thread_from_post($PostID) {
  $query = db_query("SELECT t.* FROM {f_threads} t, {f_posts} p WHERE p.PostID=%d AND t.ThreadID=p.ThreadID", $PostID);
  return db_fetch_object($query);
}

/** Returns the forum object that contains a specific thread
**/
function uieforum_get_forum_from_thread($ThreadID) {
  $query = db_query("SELECT f.* FROM {f_forums} f, {f_threads} t WHERE t.ThreadID=%d AND f.ForumID=t.ForumID", $ThreadID);
  return db_fetch_object($query);
}

/**  Returns a thread object, that corresponds to a given thread id
**/
function uieforum_get_thread($ThreadID) {
  $query = db_query("SELECT t.* FROM {f_threads} t WHERE t.ThreadID=%d", $ThreadID);
  return db_fetch_object($query);
}

/** Returns an array of threads that are contained withing a forum
*/
function uieforum_get_threads_from_forum($fid, $orderby = true, $pagelimit = true) {
  static $storageArray = array();

  if(!$storageArray[$fid]) {
    $Threads = Array();
    $q  = "SELECT t.* FROM {f_threads} t WHERE t.ForumID=%d ";
    $q .= $orderby ? "ORDER BY t.Sticky DESC, t.LastPost DESC " : "";
    $q .= $pagelimit ? "LIMIT ".uieforum_get_pages_offest().", ".variable_get('forum_per_page', 25) : "";
    $query = db_query($q, $fid);

    $Threads = uieforum_to_array($query);
    $storageArray[$fid] = $Threads;
  }
  return $storageArray[$fid];
}

/** Returns an array of all threads
*/
function uieforum_get_threads() {
  $Threads = Array();
  $query = db_query("SELECT * FROM {f_threads}");

  $Threads = uieforum_to_array($query);
  return $Threads;
}

/**
 * Deep Forum get. Adds a .Depth Property to the Forum Object
 */
function uieforum_get_deep_forums($fid, $recursionDepth = 1)
{
	$Forums = array();
	$ForumList = uieforum_get_forums($fid);
	foreach($ForumList AS &$Forum)
	{
		$Forum->Depth=$recursionDepth;
		$Forums[] = $Forum;
		if ($recursionDepth == 1 && uieforum_forum_num_children($Forum->ForumID))
		{
			$Forums = array_merge($Forums, uieforum_get_deep_forums($Forum->ForumID, ($recursionDepth+1)));
		}
	}
	return $Forums;
}

/** Returns all forums that are within a forum. (that a user has access to)
**/
function uieforum_get_forums($fid = null, $arr = false, $forcerecount = false, $includeThis = false)
{
  static $storageArray = array();
  $storageArray = array();
  if(
      ($fid && !$storageArray[$fid])
   || ($forcerecount && (!$fid && !$storageArray[$fid]->ForceRecount) )
   || !$fid
    )
  {
    $Forums = Array();

    if($fid == null && !$forcerecount) {
      $limitsql = "SELECT * FROM {f_forums} WHERE ParentForum is null OR ParentForum = 0 ";
    }
    else
    {
      if($arr) {
        sort($arr);
      }
      if($forcerecount) {
        $limitsql = "SELECT {f_forums}.ForumID, {f_forums}.ForumName, {f_forums}.ParentForum, COUNT(DISTINCT({f_threads}.ThreadID)) AS ThreadCount, COUNT(DISTINCT({f_posts}.PostID)) AS PostCount, '' AS TotalPostCount, '' AS TotalThreadCount FROM {f_forums} LEFT JOIN {f_threads} ON {f_threads}.ForumID={f_forums}.ForumID LEFT JOIN {f_posts} ON {f_posts}.ThreadID={f_threads}.ThreadID " . ( $includeThis ? "" : "WHERE {f_forums}.ParentForum='%d'");
        $limitsql .= " GROUP BY {f_threads}.ForumID";
        $limitsql .= " ORDER BY {f_forums}.ForumID ASC";
//        $limitsql .= " ORDER BY weight, ForumName ASC";
      }
      else
      {
        $limitsql = "SELECT * FROM {f_forums} f WHERE f.ParentForum=%d";
        if($arr && sizeof($arr) > 0) {
          $limitsql .= " OR f.ParentForum IN (" . implode(', ', $arr) . ") OR f.ForumID='".$fid."'";
        }

        $limitsql .= uieforum_get_allowed_forums();
        $limitsql .= " ORDER BY weight, ForumName ASC";
//        echo $limitsql;die();
      }
    }

    if(!$fid || $forcerecount)
    {
      $query = db_query($limitsql);
    }
    else
    {
      $query = db_query($limitsql, $fid);
    }
    $Forums = uieforum_to_array($query, "ForumID");
    if($forcerecount)
    {
      $maxsize = sizeof($Forums);

      foreach($Forums AS $key => &$val)
      {
        $Forums[$key]->ForceRecount = true;
      }
      return $Forums;
    }
    else
    {
      $storageArray[$fid] = $Forums;
    }
  }
  return $storageArray[$fid];
}

function uieforum_get_all_allowed_forums() {
  static $list = null;
  if(!$list) {
    $list = array();
    $forums = uieforum_get_all_forums();
    $temparr = Array();
    foreach($forums as &$f) {
      $groups = uieforum_get_security_groups($f->ForumID);
      foreach($groups AS $key => &$g) {
        if($g->HasAccess && !in_array($g->HasAccess, $list)) {
          $list[] = $g->HasAccess;
        }
      }
    }
    sort($list);
  }
  return $list;
}

/** Returns a MYSQL string to be used to restrict a select statement to only those forums that a given user has access to
**/
function uieforum_get_allowed_forums($fid = false, $return_list = false)
{
   static $basic_string, $list;

   if(isset($basic_string) && !$return_list) return $basic_string;
   elseif(isset($list) && $return_list && isset($list[$fid])) {
     return $list[$fid];
   }

   $allowed = array();

   if(!user_access('administer '.uieforum_get_module_security_name()))
   {
     $allowed = uieforum_get_all_allowed_forums();
   }
   sort($allowed);

   $alltext = "";
   if(sizeof($allowed) > 0)
   {
     $alltext = " AND f.ForumID IN(";
     $alltext .= implode(', ', $allowed);
     $alltext .= ')';
   }
   if(!$fid && !$return_list) $basic_string = $alltext;
   elseif($return_list) {
     if(!$list) {
       $list = array();
     }
     if(!$list[$fid]) {
       $list[$fid] = $allowed;
     }
     return $list[$fid];
   }
   return $alltext;
}

/** Returns the number of (direct) children that a forum has. This is NOT recursive
**/
function uieforum_forum_num_children($fid)
{
  $Forums = Array();
  $query = db_query("SELECT COUNT(ForumID) AS numchildren FROM {f_forums} WHERE {f_forums}.ParentForum=%d;", $fid);
  $Forums = uieforum_to_array($query);
  return $Forums[0]->numchildren;
}

/** Returns an array of all forums as objects.
**/
function uieforum_get_all_forums()
{
   $Forums = Array();

   $query = db_query("SELECT DISTINCT (f.ForumID), f.* FROM {f_forums} f ORDER BY f.ForumName ASC");
   $Forums = uieforum_to_array($query);
   return $Forums;
}

/** Returns a forum object that corresponds to a given forum fid
**/
function uieforum_get_forum($fid)
{
  static $forumList = array();

  if(!$forumList[$fid])
  {
    $limitsql = "SELECT * FROM {f_forums} WHERE ForumID=%d";
    $query = db_query($limitsql, $fid);
    $forumList[$fid] = db_fetch_object($query);
  }
  return $forumList[$fid];
}

/**  Returns a 2D array containing a forum object, and it's indentation int.
     Forums are listed in order of inheritance, weight, and then name.
     eg:
root
-- 1
---- 1a
-- 2
---- 2a
------ 2aa
**/
function uieforum_get_forums_indented($Forum = null, $indent = 0, $stop_recursion_on_forumid = false, $mod_only = false)
{
  static $found_vals = array();

  $big_array = array();
  if(is_int($Forum)) $Forum = uieforum_get_forum($Forum);
  $ForumList = uieforum_get_forums($Forum->ForumID);

  if($Forum != null)
  {
    if(!$mod_only || ($mod_only && user_access('moderate '.uieforum_get_module_security_name().'_'.$Forum->ForumID.'_'.uieforum_get_module_security_name())))
    {
      $big_array[] = array($Forum, $indent);
      $found_vals[] = $Forum->ForumID;
    }
  }
  if (sizeof($ForumList) >= 0)
  {
    $indent++;
    foreach($ForumList AS &$fo)
    {
      if($fo->ForumID != $stop_recursion_on_forumid)
      {
        $temp_fo = uieforum_get_forums_indented($fo, $indent, $stop_recursion_on_forumid, $mod_only);

        if(uieforum_forum_num_children($fo->ForumID) > 0)
        {
          foreach($temp_fo AS &$t)
          {
            $big_array[] = $t;
          }
        }
        elseif(!$mod_only || ($mod_only && user_access('moderate '.uieforum_get_module_security_name().'_'.$fo->ForumID.'_'.uieforum_get_module_security_name())))
          $big_array[] = array($fo, $indent);
      }
    }
  }
  return $big_array;
}

/** Returns the list of security groups when fid is false (or null)
    otherwise, it returns the list of security restrictions currently set on a given forum
    All groups listings are restricted by a user's access level
**/
function uieforum_get_security_groups($fid = false)
{
   static $groups = array();
   $key = $fid ? $fid : 'default';
   if(isset($groups[$key])) {
     return $groups[$key];
   }

   $limitsql = "SELECT g.*, ga.ForumID AS HasAccess FROM {f_groups} g LEFT JOIN {f_groups_access} ga ON g.GroupID = ga.GroupID";
   if($fid) {
     $limitsql .= " WHERE ga.ForumID=%d";
   }
   $query = db_query($limitsql, $fid);

   $query_groups = uieforum_to_array($query);
   $new_groups = array();
   foreach($query_groups AS &$g) {
     if(user_access($g->GroupName)) {
  	   $new_groups[$g->GroupID] = $g;
     }
   }
   $groups[$key] = $new_groups;
   return $groups[$key];
}

/** Returns the security group that corresponds to a given groupid
**/
function uieforum_get_security_group($pGroupID)
{
  $vSecGroupObj = new UIESecGroup();
  $found = $vSecGroupObj->GetById($pGroupID);
  return $found ? $vSecGroupObj : $found;
}

/** Adds a new security group.
    Param: $GroupName - the name of the group to add
    Param: $GroupShortName - the shortname of the group to add
**/
function uieforum_add_security_group($pGroupName, $pGroupShortName)
{
  $vSecGroupObj = new UIESecGroup();
  $vSecGroupObj->SetName($pGroupName);
  $vSecGroupObj->SetShortName($pGroupShortName);

  $vSecGroupObj->SetMode(UIEMODEL_MODE_ADD);

  $vSecGroupObj->Validate();
  return $vSecGroupObj->Maintain();
}

/** Edits an existing security group.
    Param: $GroupID - the id of the group to modify
    Param: $GroupName - the name of the group to modify
    Param: $GroupShortName - the shortname of the group to modify
**/
function uieforum_edit_security_group($pGroupID, $pGroupName, $pGroupShortName)
{
  $vSecGroupObj = new UIESecGroup();
  $vSecGroupObj->SetGid($pGroupID);
  $vSecGroupObj->SetName($pGroupName);
  $vSecGroupObj->SetShortName($pGroupShortName);

  $vSecGroupObj->SetMode(UIEMODEL_MODE_UPDATE);

  $vSecGroupObj->Validate();
  return $vSecGroupObj->Maintain();
}

/** Deletes a thread, and all posts contained therein
**/
function uieforum_delete_security_group($pGroupID)
{
  $vSecGroupObj = new UIESecGroup();
  $vSecGroupObj->SetGid($pGroupID);

  $vSecGroupObj->SetMode(UIEMODEL_MODE_DELETE);

  $vSecGroupObj->Validate();
  return $vSecGroupObj->Maintain();
}

/** Returns the last post time for a user
**/
function uieforum_get_last_post_time_for_user($uid) {
   $query = db_query("SELECT {f_posts}.Posted FROM {f_posts} WHERE {f_posts}.Poster=%d ORDER BY {f_posts}.PostID DESC LIMIT 1", $uid);
   $obj = db_fetch_object($query);
   return $obj->Posted;
}

function uieforum_get_last_forum_post_test($fid)
{
  return db_fetch_object(db_query("SELECT {users}.uid, {users}.name, {f_threads}.ThreadID, MAX(LastPost) AS LastPost FROM {f_threads}, {f_forums}, {users} WHERE {f_forums}.ForumID=%d AND {f_forums}.ForumID={f_threads}.ForumID AND {users}.uid={f_threads}.LastPoster GROUP BY {f_threads}.ThreadID ORDER BY LastPost DESC LIMIT 1", $fid));
}

/** Gets the last post in a forum
**/
function uieforum_get_last_forum_post($Forum)
{
  $ForumList = uieforum_get_forums($Forum->ForumID);
  $obj = uieforum_get_last_forum_post_test($Forum->ForumID);

  $latestpost = $obj->LastPost;
  $forumToReturn = $obj;

  if (sizeof($ForumList) != 0)
  {
    foreach($ForumList AS &$fo)
    {
      $PostThingObject = uieforum_get_last_forum_post_test($fo->ForumID);
      $PostThing = $PostThingObject->LastPost;
      if ($PostThing > $latestpost)
      {
        $latestpost = $PostThing;
        $forumToReturn = $PostThingObject;
      }
    }
  }
  return $forumToReturn;
}

/** Returns how many pages we have when using paging
**/
function uieforum_get_max_page_count($numrows) {
  return ceil($numrows/variable_get('forum_per_page', 25));
}

function uieforum_get_current_link() {
  static $self = false, $page_included = false;
  if(!$self)
  {
    $self = uieforum_get_module_menu_name();
    if (uie_id('c'))
    {
      $self .= '/'.uie_id('c').'/';
      $self .= uie_id('PostID') ? uie_id('PostID') : uie_id('ThreadID');
    }
    else
    {
      if (uie_id('fid'))
      {
        $self .= '/'.uie_id('fid');
      }
    }
  }
  return $self;
}

/** Returns a string containing page numbers for the current $page and $maxPage
**/
function uieforum_get_page_numbers($numrows) {
  $page = uie_id("page");
  $self = uieforum_get_current_link();

// print 'previous' link only if we're not on page one
  if ($page > 1)
  {
    $prevPageNumber = $page - 1;
    //No need for ?page=1, just set to /location/$id
    $prev = " ".l('&lt;', $self, null, ($prevPageNumber > 1 ? "page=$prevPageNumber" : null), null, null, true);
    $first = " ".l('<strong>&laquo;</strong>', $self, null, null, null, null, true);
  }
  elseif(isset($page) && $page < 1) {
    drupal_not_found();
    die();
  }
  else
  {
    $prev  = ' &lt; ';       // we're on page one, don't enable 'previous' link
    $first = ' <strong>&laquo;</strong> '; // nor 'first page' link
  }

// print 'next' link only if we're not on the last page
  $maxPage = uieforum_get_max_page_count($numrows);
  if ($page < $maxPage)
  {
    $nextPageNumber = $page + 1;
    $next = " ".l('&gt;', $self, null, "page=$nextPageNumber", null, null, true);
    $last = " ".l('<strong>&raquo;</strong>', $self, null, "page=$maxPage", null, null, true);
  }
  elseif(isset($maxPage) && $maxPage && $page > $maxPage) {
    drupal_not_found();
    die();
  }
  else
  {
    $next = ' &gt; ';      // we're on the last page, don't enable 'next' link
    $last = ' <strong>&raquo;</strong> '; // nor 'last page' link
  }

  $table = t("Page: !first !prev <strong>!page</strong> of <strong>!max</strong> !next !last", array('!first' => $first, '!prev' => $prev, '!page' => $page, '!max' => $maxPage, '!next' => $next, '!last' => $last));
  return $table;
}

/** Creates an array of db objects
**/
function uieforum_to_array($val, $key = false) {
  $array = array();
  if (db_num_rows($val) > 0) {
    while ( $Result = db_fetch_object($val) ) {
      $resArray = (array)($Result);
      if($key) {
        $array[$resArray[$key]] = $Result;
      }
      else {
        $array[] = $Result;
      }
    } # Loop until we have them all
  }
  return $array;
}

/** Returns an array of the last posts made on the forum module. This _IS_ secured using uieforum_get_allowed_forums()
**/
function uieforum_get_last_posts($num_posts = 10, $fid=false)
{
  $Threads = Array();
  $limitsql = "SELECT DISTINCT (f.ForumID), t.*, f.ForumName FROM {f_forums} f, {f_threads} t WHERE";

  if($fid)
    $limitsql .= " f.ForumID=%d AND ";

  $limitsql .= " f.ForumID=t.ForumID".uieforum_get_allowed_forums();
  $limitsql .= " ORDER BY LastPost DESC LIMIT ".$num_posts;
  if($fid)
    $query = db_query($limitsql, $fid);
  else
    $query = db_query($limitsql);

  $Threads = uieforum_to_array($query);
  return $Threads;
}

/** Returns a post object that corresponds to a given PostID
**/
function uieforum_get_post($PostID)
{
  $query = db_query("SELECT STRAIGHT_JOIN * FROM {f_posts},{users} WHERE PostID=%d and {f_posts}.Poster = {users}.uid", $PostID);
  return db_fetch_object($query);
}

/** Returns an array of posts that are contained with a given thread
**/
function uieforum_get_first_postid_from_thread($ThreadID)
{
  static $storageArray = array();

  if(!$storageArray[$ThreadID])
  {
    $q = "SELECT MIN(p.Posted), p.PostID FROM {f_posts} p WHERE ThreadID=%d GROUP BY ThreadID";

    $query = db_query($q, $ThreadID);
    $storageArray[$ThreadID] = uieforum_to_array($query);
  }
  return $storageArray[$ThreadID];
}

/** Returns an array of posts that are contained with a given thread
**/
function uieforum_get_posts($ThreadID, $paged = true, $ids_only = false, $reverse = false)
{
  static $storageArray = array();

  if(!$storageArray[$ThreadID] || $ids_only || $reverse)
  {
    $Posts = Array();
    $q = $ids_only ? "SELECT PostID FROM {f_posts} WHERE ThreadID=%d ORDER BY Posted ASC"
      : "SELECT * FROM {f_posts} LEFT JOIN {users} ON {f_posts}.Poster = {users}.uid WHERE ThreadID=%d ORDER BY Posted " . ($reverse ? "DESC" : "ASC");
    if($paged)
      $q .= " LIMIT ".uieforum_get_pages_offest().", ".variable_get('forum_per_page', 25);

    $query = db_query($q, $ThreadID);
    $Posts = uieforum_to_array($query);
    if($ids_only || $reverse) {
      return $Posts;
    }
    $storageArray[$ThreadID] = $Posts;
  }
  return $storageArray[$ThreadID];
}

/** Creates a thread
    Param: ThreadTitle - Title of the thread
    Param: ThreadText - Text(content) for the thread
    Param: fid - The forum to add this thread to
**/
function uieforum_create_thread($ThreadTitle, $ThreadText, $fid, $AlwaysEdit = null)
{
  Global $user;
	$result = null;
  $now = uieforum_get_now();

  if( !$ThreadTitle || strlen(trim($ThreadTitle)) <= 0 || !$fid){ trigger_error('UIEForum Error - Tried to create a thread with no title, or no forum id'); return false; }

	$query = db_query("INSERT INTO {f_threads} (ThreadID, ThreadTitle, Creator, Created, LastPoster, LastPost, Locked, Sticky, ForumID) VALUES (NULL,'%s','%d','%s','%d','%s', '%s', '%s', %d)", $ThreadTitle, $user->uid, $now, $user->uid, $now, '0', '0', $fid);
	$query = db_query("SELECT LAST_INSERT_ID() AS id");
	$result = db_fetch_object($query);

	if(!uieforum_add_post($result->id, $ThreadTitle, $ThreadText, $now, $AlwaysEdit))
  {
		$query = db_query("DELETE FROM {f_threads} WHERE ThreadID=%d LIMIT 1", $result->id);
    return false;
  }
  uieforum_update_counts($fid, "forum", "thread", true);

  return $result->id;
}

/** Splits a thread from a given post onwards (inclusive)
    Param: edit - the array containing all relevant details needed for splitting
**/
function uieforum_split_thread($PostID, $title)
{
  $vPost = uieforum_get_post($PostID);
  $vOldThread = uieforum_get_thread_from_post($PostID);

  //Create the thread
  $query = db_query("INSERT INTO {f_threads} (ThreadID, ThreadTitle, Creator, Created, LastPoster, LastPost, Locked, Sticky, ForumID) VALUES (NULL,'%s','%d','%s','%d','%s', '%s', '%s', '%d')", $title, $vPost->Poster, $vPost->Posted, $vPost->Poster, $vPost->Posted, '0', '0', $vOldThread->ForumID);
  $query = db_query("SELECT LAST_INSERT_ID() AS id");
  $result = db_fetch_object($query);

  $newThreadID = $result->id;

  $query = db_query("UPDATE {f_posts} SET ThreadID='%d' WHERE ThreadID='%d' AND PostID >= '%d'", $newThreadID, $vPost->ThreadID, $vPost->PostID);

  uieforum_update_counts($vPost->ThreadID, "thread");
  uieforum_update_counts($newThreadID, "thread");
  uieforum_update_counts($vOldThread->ForumID, "forum", "thread", true);

  uieforum_update_last_post_details($newThreadID);
  uieforum_update_last_post_details($vPost->ThreadID);
  return true;
}

function uieforum_update_last_post_details($ThreadID)
{
  //Update the last-post of a thread
  $query = db_query("SELECT * FROM {f_posts} WHERE ThreadID='%d' ORDER BY PostID DESC LIMIT 1", $ThreadID);
  $result = db_fetch_object($query);

  if(!$result->Posted) {
    $query = db_query("DELETE FROM {f_threads} WHERE ThreadID='%d'", $ThreadID);
    return true;
  }
  $query = db_query("UPDATE {f_threads} SET LastPost='%s', LastPoster='%d' WHERE ThreadID='%d'", $result->Posted, $result->Poster, $ThreadID);
  return true;
}

/** Merges 2 threads
    Param: from - the thread to merge into the parent
	Param: to - the parent
**/
function uieforum_merge_thread($from, $to)
{
  $threadFrom = uieforum_get_thread($from);

  $query = db_query("UPDATE {f_posts} SET ThreadID='%d' WHERE ThreadID='%d'", $to, $from);
  $query = db_query("DELETE FROM {f_threads} WHERE ThreadID='%d'", $from);
  uieforum_update_counts($to, "thread");
  uieforum_update_counts($to, "thread", "view", $threadFrom->ViewCount);
  uieforum_update_counts($threadFrom->ForumID, "forum", "thread", -1);

  if(uieforum_update_last_post_details($to))
    return $to;
  else
    return false;
}

/** Adds a new post
    Param: ThreadID - Thread to add the new post to
    Param: Title - Title of the post (optional)
    Param: Text - Content of the post
**/
function uieforum_add_post($ThreadID, $Title, $Text, $now = null)
{
  Global $user;
  $Thread = uieforum_get_thread($ThreadID);
  if(!$now)
  {
    $now = uieforum_get_now();
  }
  
  if( !$ThreadID || !$Text || strlen(trim($Text)) <= 0){ trigger_error('UIEForum Error - Tried to create a post with either no text or no thread id or neither'); return false; }

  $query = db_query("INSERT INTO {f_posts} (PostID, PostTitle, ThreadID, Poster, Posted, Content) VALUES (NULL,'%s','%d','%d','%s','%s')", $Title, $ThreadID, $user->uid, $now, $Text);

  $query = db_query("SELECT LAST_INSERT_ID() AS id");

  $result = db_fetch_object($query);
  $ThisPostID = $result->id;

  $query = db_query("UPDATE {f_threads} SET LastPoster='%d', LastPost='%s' WHERE ThreadID=%d", $user->uid, $now, $ThreadID);

  uieforum_update_counts($Thread->ThreadID, "thread", "post", true);
  uieforum_update_counts($Thread->ForumID, "forum", "post", true);

  return $ThisPostID;
}

/** Adds a new Forum
    Param: ForumName - Name of the forum to add
    Param: ForumDesc - Description of the forum to add
    Param: ForumParent - Parent forum of the forum to add
**/
function uieforum_add_forum($pForumName, $pForumDesc, $pForumParent, $pWeight, $pLocked, $pAccess)
{
  $vSuccess = false;
  $vForumObj = new UIEForum();
  $vForumObj->SetTitle($pForumName);
  $vForumObj->SetDescription($pForumDesc);
  $vForumObj->SetParentId($pForumParent);
  $vForumObj->SetWeight($pWeight);
  $vForumObj->SetLocked($pLocked);
  $vForumObj->SetMode(UIEMODEL_MODE_ADD);

  $vForumObj->Validate();
  $vSuccess = $vForumObj->Maintain();

  if(!$vSuccess) {
    form_set_error('UIE_FORUM_FORM', t('Unable to add new forum'));
    return false;
  }
  $vSuccess = uieforum_set_accessgroups_on_forum($vForumObj->GetFid(), $pAccess);
  return $vSuccess;
}

/** Deletes a post
**/
function uieforum_delete_post($PostID)
{
  $Post = uieforum_get_post($PostID);
  // Delete the post
  $query = db_query("DELETE FROM {f_posts} WHERE PostID=%d", $PostID);

  // Check if the thread that the post came from is empty.
  $Posts = uieforum_get_posts($Post->ThreadID, false);
  $count = count($Posts);
  if ($count <= 0)
  {
    //Delete thread as no more posts exist
    uieforum_delete_thread($Post->ThreadID);
  }
  else
  {
    //Update thread as the post that was deleted was the last one. The "lastposter" and "lastpost" database entries need to be updated
    $Post = $Posts[$count-1];
    $Thread = uieforum_get_thread($Post->ThreadID);
    $query = db_query("UPDATE {f_threads} SET LastPoster='%d', LastPost='%s' WHERE ThreadID='%d'", $Post->Poster, $Post->Posted, $Post->ThreadID);

    uieforum_update_counts($Post->ThreadID, "thread", "post", -1);
    uieforum_update_counts($Thread->ForumID, "forum", "post", -1);
  }
  return true;
}

/** Moves a post from one thread to another
**/
function uieforum_move_post($PostID, $ThreadID)
{
  $Post = uieforum_get_post($PostID);
  if( !$PostID || !$ThreadID ){ trigger_error('Unable to move post - did you select a post or a thread to move it to?'); return false;}

  $query = db_query("UPDATE {f_posts} SET ThreadID=%d WHERE PostID=%d", $ThreadID, $PostID);

  uieforum_update_last_post_details($ThreadID);
  uieforum_update_last_post_details($Post->ThreadID);

  uieforum_update_counts($Post->ThreadID, "thread", "post", -1);
  uieforum_update_counts($ThreadID, "thread", "post", true);
  return true;
}

/** Deletes a thread, and all posts contained therein
**/
function uieforum_delete_thread($ThreadID, $useTotalCounts = false)
{
  $Thread = uieforum_get_thread($ThreadID);

  db_query("DELETE FROM {f_posts} WHERE ThreadID=%d", $ThreadID);
  db_query("DELETE FROM {f_threads} WHERE ThreadID=%d", $ThreadID);

  uieforum_update_counts($Thread->ForumID, "forum", ($useTotalCounts ? "totalthread" : "thread"), -1);
  uieforum_update_counts($Thread->ForumID, "forum", ($useTotalCounts ? "totalpost" : "post"), ($Thread->PostCount)*-1);
  return true;
}

/** Deletes a forum, and all forums, threads and posts contained therein
**/
function uieforum_delete_forum($ForumID)
{
  $vSuccess = false;
  if(!$ForumID) {
    return false;
  }

  $ThisForum = uieforum_get_forum($ForumID);
  $ThisChildren = uieforum_get_forums($ForumID);
  $tester = false;

  if(sizeof($ThisChildren) > 0)
  {
    foreach($ThisChildren AS &$child)
    {
      if(!($vSuccess = uieforum_delete_forum($child->ForumID)))
        return $vSuccess;
    }
  }
  $ThisThreads = uieforum_get_threads_from_forum($ForumID);

  if(sizeof($ThisThreads) > 0)
  {
    foreach($ThisThreads AS &$tt)
    {
      if(!($vSuccess = uieforum_delete_thread($tt->ThreadID, true))) {
        return $vSuccess;
      }
    }
  }
  $query = db_query("DELETE FROM {f_groups_access} WHERE ForumID=%d", $ForumID);
  $vSuccess = db_query("DELETE FROM {f_forums} WHERE ForumID=%d", $ForumID);
  return true;
}

/** Edits a thread
    Param: ThreadID - ID of thread to edit
    Param: ThreadTitle - Titleof thread to edit
    Param: Locked - Is Thread locked?
    Param: Sticky - Is Thread stickied?
    Param: ForumID - ForumID of thread to edit
**/
function uieforum_edit_thread($ThreadID, $ThreadTitle, $pLocked, $Sticky, $ForumID)
{
  $Thread = uieforum_get_thread($ThreadID);

  $q = "UPDATE {f_threads} SET ThreadTitle='%s', Locked='%d',Sticky='%d' ";
  if($ForumID)
    $q .= ",ForumID='%d' ";
  $q .= "WHERE ThreadID='%d'";

  if($ForumID)
    $query = db_query($q, $ThreadTitle, $pLocked, $Sticky, $ForumID, $ThreadID);
  else
    $query = db_query($q, $ThreadTitle, $pLocked, $Sticky, $ThreadID);

  if($Thread->ForumID != $ForumID)
  {
    uieforum_update_counts($Thread->ForumID, "forum", "post", ($Thread->PostCount)*-1);
    uieforum_update_counts($Thread->ForumID, "forum", "thread", -1);
    uieforum_update_counts($ForumID, "forum", "post", $Thread->PostCount);
    uieforum_update_counts($ForumID, "forum", "thread", true);
  }
  return true;
}

function uieforum_edit_thread_title($ThreadID, $ThreadTitle)
{
  if(!$ThreadID || !$ThreadTitle || ($ThreadTitle && strlen(trim($ThreadTitle)) == 0))
  {
    return false;
  }
  $q = "UPDATE {f_threads} SET ThreadTitle='%s' WHERE ThreadID=%d";

  $query = db_query($q, $ThreadTitle, $ThreadID);

  return true;
}

function uieforum_set_accessgroups_on_forum($fid, $permissions) {
  if(!$fid) {
    form_set_error('UIE_FORUM_FORM', t('Unable to apply security settings - unknown forum'));
    return false;
  }
  if(!$permissions) {
    $permissions = array();
    drupal_set_message(t('Security settings wiped'), 'warning');
  }

  //Security Groups Access
  $vSecGroupAccessObj = new UIESecGroupAccess();
  $vSecGroupAccessObj->SetFid($fid);
  $vSecGroupAccessObj->SetMode(UIEMODEL_MODE_DELETE);

  $vSecGroupAccessObj->Validate();
  $vSuccess = $vSecGroupAccessObj->Maintain();

  if(!$vSuccess) {
    form_set_error('UIE_FORUM_FORM', t('Error occurred while applying initial security settings to forum'));
    return false;
  }

  $vSecGroupAccessObj->SetMode(UIEMODEL_MODE_ADD);
  foreach($permissions AS &$vAg)
  {
    if(is_array($vAg)) { continue; }
    $vSecGroupAccessObj->SetGid($vAg);
    $vSecGroupAccessObj->Validate();
    $vSuccess = $vSecGroupAccessObj->Maintain();

    if (!$vSuccess) {
      break;
    }
    $vSecGroupAccessObj->SetGid(null);
  }
  if (!$vSuccess) {
    form_set_error('UIE_FORUM_FORM', t('Error occurred while applying security settings to forum'));
    return false;
  }
  return true;
}

/** Edits a forum
**/
function uieforum_edit_forum($pFid, $pForumName, $pForumDesc, $pForumParent = false, $pWeight = 0, $pLocked, $pAccessGroups = false)
{
  $vSuccess = false;
  $vForumObj = new UIEForum();
  $vForumObj->SetFid($pFid);

  $vForumObj->SetMode(UIEMODEL_MODE_SEARCH);
  $vForumObj->Validate();
  $vResult = $vForumObj->Search();

  if($vResult) {
    $vForumObj->InitFromResults($vResult);
  }
  else {
    $vForumObj->AddError('No such forum exists');
    return false;
  }
  if(!$vForumObj->NoErrors()) {
    return false;
  }

  $vForumObj->SetTitle($pForumName);
  $vForumObj->SetDescription($pForumDesc);
  $vForumObj->SetParentId($pForumParent);
  $vForumObj->SetWeight($pWeight);
  $vForumObj->SetLocked($pLocked);
  $vForumObj->SetMode(UIEMODEL_MODE_UPDATE);

  $vForumObj->Validate();
  $vSuccess = $vForumObj->Maintain();

  if(!$vSuccess) {
    form_set_error('UIE_FORUM_FORM', t('Unable to edit forum'));
    return false;
  }

  $vSuccess = uieforum_set_accessgroups_on_forum($pFid, $pAccessGroups);
  return $vSuccess;
}

/** Edits a post
**/
function uieforum_edit_post($PostID, $Title, $Text, $editor, $reason, $ThreadID, $AlwaysEdit = null)
{
  $current_forum = uie_id('current_forum');
  if(!uieforum_is_admin_or_mod($current_forum->ForumID)) {
    $AlwaysEdit = null;
  }
  $now = uieforum_get_now();

  if(isset($AlwaysEdit)) {
    $query = db_query("UPDATE {f_posts} SET PostTitle='%s', Content='%s', Edit='%s', Editor='%d', EditReason='%s', AlwaysEdit='%d' WHERE PostID=%d", $Title, $Text, $now, $editor, $reason, $AlwaysEdit, $PostID);
  }
  else {
    $query = db_query("UPDATE {f_posts} SET PostTitle='%s', Content='%s', Edit='%s', Editor='%d', EditReason='%s' WHERE PostID=%d", $Title, $Text, $now, $editor, $reason, $PostID);
  }
  $firstPost = uieforum_get_first_postid_from_thread($ThreadID);
  if($firstPost && count($firstPost) > 0)
  {
    if($firstPost[0]->PostID == $PostID)
    {
      if(!uieforum_edit_thread_title($ThreadID, $Title)) {
        drupal_set_message(t('Unable to set thread title'), 'error');
      }
    }
  }
  return true;
}

/** Returns the postcount of a user
**/
function uieforum_post_count($UserID)
{
  global $user;
  if (!isset($user->uid)) { return NULL; }

  $query = db_query("SELECT COUNT(PostID) from {f_posts} WHERE Poster=%d", $UserID);
  return db_result($query, 0);
}

function uieforum_apply_filters($text) {
  $filtersEnabled = variable_get('uieforum_enabled_filters', array('uieforum/0') );
  $filtersAvailable = filter_list_all();

  foreach($filtersEnabled as &$f) {
    if( array_key_exists( $f, $filtersAvailable) ) {
      $module = $filtersAvailable[$f]->module;
      if($module === 'uieforum') continue;
      $delta = $filtersAvailable[$f]->delta;

      $fname = "{$module}_filter";

      $text = $fname('prepare', $delta, -1, $text);
      $text = $fname('process', $delta, -1, $text);
    }
  }
  return $text;
}

/** Displays a post on screen
**/
function uieforum_display_post($post, $postcount = -1, $is_preview = false, $is_admin_or_mod = false, $options = array()) {
//$PostTime = 0, $ThreadID, $PostID, $uid, $PostTitle, $PostText, $postcount = -1, $IsPreview = false, $fid, $editedpost = false, $sig = false, $show_avatar = true, $show_online_status = true, $show_post_count = true, $show_ranks = true, $show_regd = true, $show_sigs = true)

  Global $user;
  $guest_user = (!isset($post->Poster) || $post->Poster == 0 ? true : false);
  
  if($post->Posted == 0) {
    $post->Posted = uieforum_get_now();
  }
  $post->PostedStamp = uieforum_parse_date($post->Posted);

  $post_header = array(
    array('data' => '', 'colspan' => '1', 'class' => 'created postHeader'),
    array('data' => '', 'colspan' => '1', 'class' => 'forum_pagenums')
  );
  $post_content = array(
    array('data' => '', 'colspan' => '1', 'class' => 'post_user_info'),
    array('data' => '', 'colspan' => '1', 'class' => 'post_text')//, 'style' => 'vertical-align: top')
  );

  if ($is_preview)
  {
    $post_header = array($post_header[0]);
    $post_content = array($post_content[1]);
    $post_footer = array($post_header[0]);
    $post_header [0]['colspan'] = '2';
    $post_header [0]['data'] = uieforum_filter('process', null, null, $post->PostTitle, true);
    $post_content[0]['data'] .= uieforum_apply_filters( uieforum_filter('process', null, null, $post->Content, true) );
    $post_content[0]['colspan'] = 2;
  }
  else
  {
    $post_footer = array(
      array('data' => '', 'colspan' => '1', 'class' => 'created postFooter'),
      array('data' => '', 'colspan' => '1', 'class' => 'forum_pagenums')
    );

    $post_header [0]['data'] = $post->PostedStamp;
    $Poster = user_load(array('uid' => $post->Poster));


    $post_header [1]['data'] .= l($post->PostID, uieforum_get_module_menu_name().'/post/'.$post->PostID, array('name' => $post->PostID));

    if ($postcount == 0) $post_header[1]['data'] .= l(null, null, array('name' => 'lastpost'));

    if($is_admin_or_mod) {
      $post_header [1]['data'] .= drupal_get_form("uieforum_generate_post_admin_form", $post->PostID, $post->ThreadID);
    }

    $UserName = $Poster->name;
    $post_content[0]['data'] .= theme('username', user_load(array('uid' => $post->Poster)))."<br /><small>\n";
    if(!$guest_user)
    {
      if($options['ranks'])
      {
	      if($Poster->uid == 1)
          $userrank = 'Super Admin';
	      else
          $userrank = uieforum_user_rank(uieforum_uie_storage('postcount', $Poster->uid));
        if($userrank != null)
          $post_content[0]['data'] .= "$userrank<br /><br />\n";
      }
      if(variable_get('uieforum_allow_display_avatars', 1) && $options['avatar']) $post_content[0]['data'] .= theme_user_picture($Poster);
      if($options['registered']) $post_content[0]['data'] .= "<br />Joined: ".uieforum_parse_date_only($Poster->created);

      if($options['post_counts'])
      {
        $userpostcount = uieforum_uie_storage('postcount', $Poster->uid);
        if($userpostcount != null)
          $post_content[0]['data'] .= "<br/>Posts: $userpostcount<br /><br />";
      }
      if($options['online_status'])
      {
        $is_active = uieforum_uie_storage('useronline', $Poster->uid);
        $post_footer[0]['data'] .= _uieforum_user_online($Poster->name, $is_active);
      }
      if($user->uid || variable_get('forum_report_post_guest_allow', 0))
      {
        $alt = 'Report post to an Admin/Moderator';
        $file = variable_get('forum_icon_path', 'sites/all/modules/'.uieforum_get_module_name().'/uie')."/forum-report.gif";
        $reportPostImage = theme('image', $file, $alt, $alt, '', false);

        if($reportPostImage)
        {
          $reportPost .= l($reportPostImage, uieforum_get_module_menu_name().'/post/'.$post->PostID.'/report', null, null, null, null, true);
          $post_footer[0]['data'] .= $reportPost;
        }
      }
      if (module_exists('privatemsg') && $user->uid != $Poster->uid && user_access('access private messages')) {
        $icon = variable_get('forum_icon_path', 'sites/all/modules/'.uieforum_get_module_name().'/uie')."/forum-send_message.png";
        $alt = t("Message me");
        $post_footer[0]['data'] .= l(theme('image', $icon, $alt, $alt), 'privatemsg/new/'.$Poster->uid, null, null, null, null, true);
      }
    }
    $post_content[0]['data'] .= "</small>\n";


    if (strlen($post->PostTitle) > 0)
      $post_content[1]['data'] .= "<div class='uieforum_postsubject'>".uieforum_filter('process', null, null, $post->PostTitle, true)."</div>";
//    $post_content[1]['data'] .= "<div class='uieforum_postcontent'>".uieforum_filter('process', null, null, $post->Content, true)."</div>";
    $post_content[1]['data'] .= "<div class='uieforum_postcontent'>".uieforum_apply_filters( uieforum_filter('process', null, null, $post->Content, true) )."</div>";
    $post_content[1]['data'] .= "<br/><br/>";
    if(!$guest_user && ($options['sigs'] && $post->signature))
      $post_content[1]['data'] .= "<div class=\"signature\">__________________<br/>".uieforum_filter('process', null, null, $post->signature)."</div>";



    $INTERACTION_TEXT = false;

    if (user_access('post new threads/posts'))
    {
      if($is_admin_or_mod || (!uieforum_edit_expired($post) && $user->uid == $post->Poster && $user->uid != 0)) {
        $INTERACTION_TEXT = l(t('Edit'), uieforum_get_module_menu_name()."/post/$post->PostID/edit"). " - ";
      }
      $INTERACTION_TEXT .= l(t('Quote'), uieforum_get_module_menu_name()."/post/$post->PostID/quote");
    }
    if($INTERACTION_TEXT) $post_footer[1]['data'] .= $INTERACTION_TEXT;

    if($post->Editor || $post->Edit || $post->EditReason) {
      $tempuser = user_load(array('uid' => $post->Editor));
      $DEFAULT_EDIT_REASON = trim(uieforum_filter('process', null, null, variable_get('forum_default_edit_reason', ''), true));
      $outputtext = "Last edited by ".$tempuser->name." (".uieforum_parse_date($post->Edit).")";

      $edited_reason = uieforum_filter('process', null, null, $post->EditReason, true);
      if(strlen(trim($edited_reason)) <= 0) $edited_reason = $DEFAULT_EDIT_REASON;
      if(strlen($edited_reason) > 0)
        $outputtext .= " Reason: ".$edited_reason;
      $post_content[1]['data'] .= "<hr /><div class='uie_edit_text'><em>".$outputtext."</em></div>";
    }
  }
  return array($post_header, $post_content, $post_footer);
}

/** Returns the current date/time
**/
function uieforum_get_now($days=0)
{
  $Now = time();
  $Time = $Now - (86400*$days); # 86400 = num seconds in a day.
	return $Time;
}

/** Not used at the moment - returns a different date format for rss display
**/
function uieforum_parse_date_rss($d)
{
  return uieforum_parse_date($d, "D, d M Y H:i");
}

/**Returns a formatted date/time
**/
function uieforum_parse_date($d, $format = "d M Y - H:i") {
  return format_date($d, 'custom', $format);
}

/** Returns a formatted date
**/
function uieforum_parse_date_only($d) {
  return uieforum_parse_date($d, "d M Y");
}

/** Checks to see if a user has posted too soon since their last post.
**/
function uieforum_posted_too_soon($timeout) {
  Global $user;
  $time = time();

  $userslastpost = uieforum_get_last_post_time_for_user($user->uid);

  $ender = $time - $userslastpost;
  if($ender <= $timeout) {
    return $ender;
  }
  else {
    return false;
  }
}

/** Returns a smiley array
**/
function uieforum_get_smiley_list()
{
  static $smilies_array = array();

  if(sizeof($smilies_array) == 0)
  {
    /** Array of smilies
      contents: text replace   ---   regex to use to replace   ---   what to replace text with
      eg: ':)', ':) regex', 'smile.gif'
    **/
    $smilies_array = array
    (
      array(':idea:', 	'/:(?i)idea:/' ,	_uieforum_icon(null, null, null, 'idea.gif', true)),
      array(':mad:', 	'/:(?i)mad:/'  ,	_uieforum_icon(null, null, null, 'mad.gif', true)),
      array(':huh:', 	'/:(?i)huh:/'  ,	_uieforum_icon(null, null, null, 'confused.gif', true)),
      array(':lol:', 	'/:(?i)lol:/'  ,	_uieforum_icon(null, null, null, 'lol.gif', true)),
/*      array(':))'  , 	'/:-?\)\)/'    ,	_uieforum_icon(null, null, null, 'lol.gif', true)),*/
      array(':)'   , 	'/:-?\)/'      ,	_uieforum_icon(null, null, null, 'smile.gif', true)),
      array(':D'   , 	'/:-?[dD]/'    ,	_uieforum_icon(null, null, null, 'biggrin.gif', true)),
      array(':pirateship:',	'/:(?i)pirateship:/',	_uieforum_icon(null, null, null, 'pirateship.gif', true)),
      array(':p'   , 	'/:-?[pP]/'    ,	_uieforum_icon(null, null, null, 'tongue.gif', true)),
      array(':('   , 	'/:-?\(/'      ,	_uieforum_icon(null, null, null, 'sad.gif', true)),
      array(':cool:',	'/:(?i)cool:/' ,	_uieforum_icon(null, null, null, 'cool.gif', true)),
      array(':crazy:',	'/:(?i)crazy:/',	_uieforum_icon(null, null, null, 'crazy.gif', true)),
      array(':fight:',	'/:(?i)fight:/',	_uieforum_icon(null, null, null, 'fight.gif', true)),
      array(':lurking:',	'/:(?i)lurking:/',	_uieforum_icon(null, null, null, 'lurking.gif', true)),
      array(':rolleyes:',	'/:(?i)rolleyes:/',	_uieforum_icon(null, null, null, 'rolleyes.gif', true)),
      array(':shock:',	'/:(?i)shock:/',	_uieforum_icon(null, null, null, 'shockcombo.gif', true)),
      array(':shoot:',	'/:(?i)shoot:/',	_uieforum_icon(null, null, null, 'shooting.gif', true)),
      array(':sleeping:',	'/:(?i)sleeping:/',	_uieforum_icon(null, null, null, 'sleeping.gif', true)),
      array(':|',    	'/:-?\|/'      ,	_uieforum_icon(null, null, null, 'stern.gif', true)),
      array(';)',    	'/;-?\)/'      ,	_uieforum_icon(null, null, null, 'wink.gif', true)),
    );
  }
  return $smilies_array;
}

/** Order to display smilies in **/
function uieforum_get_smiley_samples()
{
  static $SmilieSamples = array();

  if(sizeof($SmilieSamples) == 0)
  {
    $SmilieSamples = array
    ( 0 => array ( 5, 1, 13, 11),
      1 => array ( 7, 2, 10, 14),
      2 => array ( 4, 3, 17, 12),
      3 => array ( 9, 8, 16, 15),
      4 => array (18)
    );
  }
  return $SmilieSamples;
}

/** Returns a formatting icon array
**/
function uieforum_get_formatting_icons()
{
  $uie_rows = array();
	if(variable_get("uieforum_create_post_display_bbcode_bold", 1))
	{
    $uie_rows[] = array('data' => '<a href="javascript:Process(\'bold\')" alt=\'Insert bold text\' title=\'Insert bold text\'>'._uieforum_icon(null, null, null, 'bold.gif', null, true).'</a>');
	}
	if(variable_get("uieforum_create_post_display_bbcode_italic", 1))
	{
    $uie_rows[] = array('data' => '<a href="javascript:Process(\'italic\')" alt=\'Insert italic text\' title=\'Insert italic text\'>'._uieforum_icon(null, null, null, 'italic.gif', null, true).'</a>');
	}
	if(variable_get("uieforum_create_post_display_bbcode_underline", 1))
	{
    $uie_rows[] = array('data' => '<a href="javascript:Process(\'underline\')" alt=\'Insert underlined text\' title=\'Insert underlined text\'>'._uieforum_icon(null, null, null, 'underline.gif', null, true).'</a>');
	}
	if(variable_get("uieforum_create_post_display_bbcode_url", 1))
	{
    $uie_rows[] = array('data' => '<a href="javascript:Process(\'url\')" alt=\'Insert a url\' title=\'Insert a url\'>'._uieforum_icon(null, null, null, 'url.gif', null, true).'</a>');
	}
	if(variable_get("uieforum_create_post_display_bbcode_image", 1))
	{
    $uie_rows[] = array('data' => '<a href="javascript:Process(\'image\')" alt=\'Insert an inline image\' title=\'Insert an inline image\'>'._uieforum_icon(null, null, null, 'image.gif', null, true).'</a>');
	}
	if(variable_get("uieforum_create_post_display_bbcode_quote", 1))
	{
    $uie_rows[] = array('data' => '<a href="javascript:Process(\'quote\')" alt=\'Insert quoted text\' title=\'Insert quoted text\'>'._uieforum_icon(null, null, null, 'quote.gif', null, true).'</a>');
	}
	if(variable_get("uieforum_create_post_display_bbcode_code", 1))
	{
    $uie_rows[] = array('data' => '<a href="javascript:Process(\'code\')" alt=\'Insert pre-formatted code\' title=\'Insert pre-formatted code\'>'._uieforum_icon(null, null, null, 'code.gif', null, true).'</a>');
	}
	if(variable_get("uieforum_create_post_display_bbcode_rant", 1))
	{
    $uie_rows[] = array('data' => '<a href="javascript:Process(\'rant\')" alt=\'Give out about something\' title=\'Give out about something\'>'._uieforum_icon(null, null, null, 'rant.gif', null, true).'</a>');
	}
	if(variable_get("uieforum_create_post_display_bbcode_left", 1))
	{
    $uie_rows[] = array('data' => '<a href="javascript:Process(\'left\')" alt=\'Align paragraph to left\' title=\'Align paragraph to left\'>'._uieforum_icon(null, null, null, 'align_left.jpg', null, true).'</a>');
	}
	if(variable_get("uieforum_create_post_display_bbcode_center", 1))
	{
    $uie_rows[] = array('data' => '<a href="javascript:Process(\'center\')" alt=\'Align paragraph to center\' title=\'Align paragraph to center\'>'._uieforum_icon(null, null, null, 'align_center.gif', null, true).'</a>');
	}
	if(variable_get("uieforum_create_post_display_bbcode_right", 1))
	{
    $uie_rows[] = array('data' => '<a href="javascript:Process(\'right\')" alt=\'Align paragraph to right\' title=\'Align paragraph to right\'>'._uieforum_icon(null, null, null, 'align_right.jpg', null, true).'</a>');
	}
  return array($uie_rows);
}

function uieforum_get_pages_offest()
{
  static $pagesoffset = 0;

  if(is_numeric($pagesoffset) && $pagesoffset <= 0)
    $pagesoffset = ((uie_id("page") > 0 ? uie_id("page") - 1 : 0) ) * variable_get('forum_per_page', 25);
  return $pagesoffset;
}

function uieforum_get_forum_list_header()
{
  static $forum_forum_list_header = array();

  if(sizeof($forum_forum_list_header) == 0)
  {
    $forum_forum_list_header = array(
      array('data' => '&nbsp;', 'class' => 'created', 'class' => 'uieforumicon uieforum_nopadding'),
      array('data' => t('Forum'), 'class' => 'created uieheader'),
      array('data' => t('Topics'), 'class' => 'numtopics uieheader'),
      array('data' => t('Posts'), 'class' => 'numposts uieheader'),
      array('data' => t('Last Post'), 'class' => 'uieforum-last-reply uieheader')
    );
  }
  return $forum_forum_list_header;
}

function uieforum_get_topic_list_header()
{
  static $forum_topic_list_header = array();

  if(sizeof($forum_topic_list_header) == 0)
  {
    $forum_topic_list_header = array(
      array('data' => '&nbsp;', 'class' => 'uieforumicon uieforum_nopadding'),
      array('data' => t('Thread'), 'class' => 'created uieheader'),
      array('data' => t('Replies'), 'class' => 'numposts uieheader'),
      array('data' => t('Views'), 'class' => 'numposts uieheader'),
      array('data' => t('Last Post'), 'class' => 'uieforum-last-reply uieheader')
    );
  }
  return $forum_topic_list_header;
}

/**
 * Generates a 'userbar' containing navigation, user name, last visit, PM-box ...
 */
function uieforum_get_forum_user_header($breadcrumbs = "")
{
  global $user;

  if (count($breadcrumbs) < 1) {
  	//# assuming we only have text, so we show it.
  	$navCell = array('data' => $breadcrumbs, 'class' => 'navigation');
  } elseif (count($breadcrumbs) <= 2)  {
  	//# hide the 'Home' entry
		array_shift($breadcrumbs);
  	//# only one entry, don't show any trail
  	$navCell = array('data' => '<span class="current">'.$breadcrumbs[0].'</span>', 'class' => 'navigation');
  } else {
  	//# hide the 'Home' entry
		array_shift($breadcrumbs);
  	//# extract the last entry for big ol' display
  	$current = array_pop($breadcrumbs);
  	//# build the trail with the remains
  	$trail = '<span class="trail">'.implode($breadcrumbs, "&nbsp;>&nbsp;").'&nbsp;>&nbsp;</span>';
  	$trail .= "<br />";
  	$trail .= '<span class="current">'.$current.'</span>';
  	$navCell = array('data' => $trail, 'class' => 'navigation');
  }
  
  //# Create Welcome content
  if ($user->uid) {
    $welcome  = '<span class="welcomeText">'.t('Welcome')." {$user->name}.</span><br />";
    $welcome .= t('Last visit').": ".uieforum_parse_date($user->access)."<br />";
    //# Check for privatemsg installation
    if (module_exists('privatemsg')) {
      $msgNew = _privatemsg_get_new_messages();
      $msgTotal = _privatemsg_get_messages();
      $welcome .= l(t("Private messages: "),'privatemsg/inbox').$msgNew." of ".$msgTotal." total.";
    }
  } else {
    if (variable_get('user_register', 1)) {
      $welcome = t('<a href="@login">Login</a> or <a href="@register">register</a> to post.', array('@login' => url('user/login'), '@register' => url('user/register')));
    }
    else {
      $welcome = t('<a href="@login">Login</a> to post comments', array('@login' => url('user/login')));
    }
  }
  

  
  //# Generate Table
  $table = array(
  	array(
    	$navCell,
    	array('data' => $welcome, 'class' => 'welcome'),
    )
	);
	$result = theme('table', null, $table, array('class' => 'forum userbar'));
	
	if (uie_id('c') == 'thread') 
	{
		$result = "<div class=\"post-container\">".$result."</div>";
	}
	
  return $result;
}

//# The API of privatemsg isn't done, so we do it on our own.
function _privatemsg_get_messages($uid = 0) {
  global $user;
  static $cache = array();
  if ($uid == 0) {
    $uid = $user->uid;
  }
  if (!isset($cache[$uid])) {
    $cache[$uid] = (int)db_result(db_query('SELECT COUNT(*) FROM {privatemsg} WHERE recipient = %d AND recipient_del = 0', $uid));
  }
  return $cache[$uid];
}

function uieforum_get_thread_post_count($threadid)
{
  $query = db_fetch_object(db_query("SELECT COUNT(DISTINCT({f_posts}.PostID)) AS PostCount FROM {f_posts} WHERE {f_posts}.ThreadID=%d", $threadid));
  return $query->PostCount;
}

/**  Returns the number of threads/posts in a forum
**/
function uieforum_get_forum_info($init = false, $checkThisParent = false, $withThisForum = false)
{
  static  $checked = array(),
          $storageArray = array(),
          $list = array(),
          $masterOrder = array(),
          $masterOrderManipulation = array();

  if($init)
  {
    $masterOrder = uieforum_get_forums_indented();
    usort($masterOrder, 'uieforum_sort_forums_for_recounts');
    $masterOrderManipulation = $masterOrder;

    $list = uieforum_get_forums(null, null, true, true);
  }
  if($checkThisParent)
  {
    foreach($masterOrderManipulation AS $key => &$fn)
    {
      if(is_array($fn)) {
        $fn = $fn[0];
      }
      $key = $fn->ForumID;
      if($withThisForum)
      {
        $fn = $list[$withThisForum];
        $key = $withThisForum;
      }
      if($fn->ParentForum == $checkThisParent || $withThisForum)
      {
        $list[$checkThisParent]->TotalThreadCount += $fn->ThreadCount;
        $list[$checkThisParent]->TotalPostCount += $fn->PostCount;

        return $list[$checkThisParent]->ParentForum;
      }
    }
    return null;
    $key = $fn = null;
  }


  foreach($masterOrder AS $key => &$fn)
  {
    $fn = $fn[0];
    $key = $fn->ForumID;
    if(!$checked[$key] )
    {
      $checked[$key] = 1;

      $key2 = $key;

      while( $key2 = uieforum_get_forum_info(null, $key2, $key) )
      {}
    }
  }

  return $list;
}

function uieforum_update_all_counts($type, $fid = false, $tid = false)
{
  if(user_access('administer '.uieforum_get_module_security_name()))
  {
    if($type == "forums") {
      $q = "UPDATE {f_forums} SET ThreadCount=0, PostCount=0, TotalThreadCount=0, TotalPostCount=0";
      $query = db_query($q);
      $Forums = uieforum_get_forum_info(true);

      $vSuccess = uieforum_update_counts($Forums, "forum");
    }
    elseif($type == "threads") {
      if($fid) {
        $Threads = uieforum_get_threads_from_forum($fid, false, false);
      }
      elseif($tid) {
        $Threads = array();
        $Threads[] = uieforum_get_thread($tid);
      }
      else {
        $Threads = uieforum_get_threads();
      }

      foreach($Threads as &$t) {
        uieforum_update_counts($t->ThreadID, "thread");
      }
    }
  }
  return;
}

function uieforum_update_counts($id, $type, $counttype = false, $increment = false, $current_forum = true) {
  switch($type) {
    case 'forum':
      if($increment == 0) {
        //In this case, "id" is an array of all the forums in the system, with their counts pre-calculated
        if($id) {
          foreach($id AS &$f) {
            $q = "UPDATE {f_forums} SET ThreadCount='%d', PostCount='%d', TotalThreadCount='%d', TotalPostCount='%d' WHERE ForumID=%d";
            $query = db_query($q, $f->ThreadCount, $f->PostCount, $f->TotalThreadCount, $f->TotalPostCount, $f->ForumID);
          }
        }
        return;
      }
      elseif($counttype) {
        $f = uieforum_get_forum($id);
        if($counttype == "thread" || $counttype == "totalthread") {
          $q = "UPDATE {f_forums} SET TotalThreadCount='%d' WHERE ForumID=%d";
          $query = db_query($q, $f->TotalThreadCount + $increment, $id);

          if($current_forum && $counttype != "totalthread") {
            $q = "UPDATE {f_forums} SET ThreadCount='%d' WHERE ForumID=%d";
            $query = db_query($q, $f->ThreadCount + $increment, $id);
          }
        }
        elseif($counttype == "post" || $counttype == "totalpost") {
          $q = "UPDATE {f_forums} SET TotalPostCount='%d' WHERE ForumID=%d";
          $query = db_query($q, $f->TotalPostCount + $increment, $id);

          if($current_forum && $counttype != "totalpost") {
            $q = "UPDATE {f_forums} SET PostCount='%d' WHERE ForumID=%d";
            $query = db_query($q, $f->PostCount + $increment, $id);
          }
        }
      }
      else {
        return false;
      }
      if($f->ParentForum) {
        // Prevent counts of threads and posts from bubbling up to parent forum.
        // Increment totalthread and totalpost (which are SUPPOSED to include all subforums) only.
				// Fixed with thanks to Vallenwood
        if ($counttype == "thread") $counttype = 'totalthread';
        if ($counttype == "post") $counttype = 'totalpost';
        uieforum_update_counts($f->ParentForum, $type, $counttype, $increment);
      }
      break;

    case 'thread':
      $t = uieforum_get_thread($id);
      $field = "PostCount";

      if($increment == 0) {
        $info = uieforum_get_thread_post_count($id);
        if($info < 0) {
          $info = 0;
        }
      }
      elseif($counttype) {
        if($counttype == "post") {
          $info = $t->PostCount + $increment;
        }
        if($counttype == "view") {
          $field = "ViewCount";
          $info = $t->ViewCount + $increment;
        }
      }
      else {
        return false;
      }
      $q = "UPDATE {f_threads} SET ".$field."='%d' WHERE ThreadID=%d";
      $query = db_query($q, $info, $id);
      break;

    default:
  }
}


function uieforum_uie_storage($source, $uid, $reset = false) {
  static $postcount_list = array();
  static $useronline_list = array();
  if($reset) {
    $postcount_list = array();
    $useronline_list = array();
  }
  else {
    switch($source) {
      case 'postcount':
        if(!isset($postcount_list["'".$uid."'"])) {
          $postcount_list["'".$uid."'"] = uieforum_post_count($uid);
        }
        return $postcount_list["'".$uid."'"];

      case 'useronline':
        if(!isset($useronline_list["'".$uid."'"])) {
          // Count users with activity in the past defined period.
          $time_period = variable_get('user_block_seconds_online', 2700);
          $calc = time() - $time_period;

          // Perform database queries to gather online user lists (used to see if the poster is currently online)
          $users = db_query('SELECT DISTINCT(uid), MAX(timestamp) AS max_timestamp FROM {sessions} WHERE timestamp >= %d AND uid = %d GROUP BY uid ORDER BY max_timestamp DESC', $calc, $uid);
          $total_users = db_num_rows($users);

          $is_active = false;
          if ($total_users == 1) {
            $is_active = true;
          }
          $useronline_list["'".$uid."'"] = $is_active;
        }
        return $useronline_list["'".$uid."'"];
    }
  }
}

/* Reverse order, depending on their forum depth */
function uieforum_sort_forums_for_recounts($a, $b) {
  $a_Obj = $a[0];
  $b_Obj = $b[0];

  $a_Depth = $a[1];
  $b_Depth = $b[1];

  if ($a_Depth == $b_Depth) {
    return 0;
  }
  return ($a_Depth < $b_Depth) ? 1 : -1;
}

function uieforum_generate_post_admin_form($PostID, $ThreadID) {
	$dropdownmenu = array(
		null => t($PostID),
		url(uieforum_get_module_menu_name().'/post/'.$PostID.'/delete') => t('Delete Post'),
		url(uieforum_get_module_menu_name().'/post/'.$PostID.'/move') => t('Move Post'),
		url(uieforum_get_module_menu_name().'/post/'.$PostID.'/split') => t('Split Thread'),
		url(uieforum_get_module_menu_name().'/thread/'.$ThreadID.'/merge') => t('Merge Thread')
	);

	$forum_TempForm = null;
	$unique = "admin_edit_$PostID";
	$forum_TempForm[$unique] = array(
		'#type' => 'select',
		'#title' => null,
		'#default_value' => $PostID,
		'#options' => $dropdownmenu,
		'#description' => null,
		'#attributes' => array('onChange' => "forum_admin_edit('$unique', this)"),
		'#id' => $unique
	);

	$forum_TempForm['#method'] = 'post';
	$forum_TempForm['#action'] = null;
	$forum_TempForm['#attributes'] = array('name' => $unique);
	return $forum_TempForm;
}

function uieforum_get_user_post_display_options() {
  static $options = false;
  if(!$options) {
    $options['online_status'] = variable_get('uieforum_online_status', 1);
    $options['post_counts'] = variable_get('uieforum_user_post_counts', 1);
    $options['ranks'] = variable_get('uieforum_ranks', 1);
    $options['registered'] = variable_get('uieforum_show_registered', 1);
    $options['avatar'] = variable_get('user_pictures', 0);
    $options['sigs'] = variable_get('uieforum_show_sigs', 1);
  }
  return $options;
}
